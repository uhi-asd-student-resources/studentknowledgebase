"""
Script to build the docs out of markdown files and place
them in a simple HTML theme container
"""
import os
import json
import shutil
from operator import itemgetter, attrgetter
import markdown2

build_dir = "build"
docs_dir = "docs"
config_file = "config.json"
image_dir = "img"
themes_dir = "themes"

def convert_filename_to_title(filename):
    name = os.path.splitext(os.path.basename(filename))[0]
    name = name.replace("-", " ")
    name = name.replace("_", " ")
    name = name.title()
    name = name.replace("Ibm ", "IBM ")
    return name


class Section:
    def __init__(self, theme):
        self.name = None
        self.config = {
            "orderby": ["name", "asc"]
        }
        self.config_file = None
        self.items = []
        self.theme = theme

    def set_config(self, file:str):
        self.config_file = file

    def add_section(self, section):
        self.items.append(section)

    def add(self, src, dest ):
        self.items.append(Transformation(self, src,dest))

    def presentation_name(self):
        pass

    def before(self):
        print("Section::before {}".format(self.name))
        if self.config_file:
            with open(self.config_file,"r") as in_file:
                self.config.update(json.load(in_file))
        # if self.config["orderby"][0] == "title":
        #     if self.config["orderby"][1] == "desc":
        #         sorted(self.items, key=attrgetter('title'), reverse=True)
        #     else:
        #         sorted(self.items, key=attrgetter('title'), reverse=False)
        if "title" in self.config:
            self.name = self.config["title"]

        for item in self.items:
            item.before()

    def links_entry(self,selected,depth):
        print("links_entry::Generating links for section {} {}".format(depth, self.name))
        section_html=""
        section_html += "<li><div class='collapsible-header'>"+self.name+"</div></li>\n"
        section_html += "<li><div class='collapsible-body'>\n"
        section_html += "<ul>\n"
        for item in self.items:
            section_html += item.links(selected, depth)
        section_html += "</ul>\n"
        section_html += "</div></li>\n"
        return section_html

        

    def links(self, selected, depth):
        print("Generating links for section {} {}".format(depth, self.name))
        section_html=""
        section_html += "<li>\n"
        section_html += "<ul class='collapsible'>\n"
        section_html += "<li><div class='collapsible-header'><span>"+self.name+"</span></div></li>\n"
        section_html += "<li><div class='collapsible-body'>\n"
        section_html += "<ul class='collapsible-list'>\n"
        for item in self.items:
            section_html += item.links(selected, depth)
        section_html += "</ul>\n"
        section_html += "</div></li></ul>\n"
        section_html += "</li>\n"
        return section_html

    def html(self, sidebar, depth):    
        print("Generating HTML for section {}".format(self.name))
        for item in self.items:
            item.html(self.theme.sidebar, depth+1)


class Transformation:
    def __init__(self, section, src:str, dest:str):
        self.section = section
        self.src=src
        self.dest = dest
        self.m_filename = os.path.splitext(os.path.basename(self.dest))[0]
        self.m_name = convert_filename_to_title(self.m_filename)
        self.m_title = None

    def before(self):
        print("Transformation::before {}".format(self.src))
        #pass

    def href(self, depth: int):
        relative = ""
        for _ in range(0,depth-1):
            relative += "../"
        return relative+self.dest[len(build_dir)+1:]

    def filename(self):
        return self.m_filename

    def name(self):
        return self.m_name

    def title(self):
        if self.m_title is None:
            with open(self.src, "r") as in_file:
                line = in_file.readline()
                while line:
                    if line[0] == '#':
                        break
                in_file.close()
                line = line[1:]
                self.m_title = line.lstrip("#").strip()
            if self.m_title is None:
                self.m_title = self.m_name
        return self.m_title


    def links(self, selected: bool, depth: int):
        """
        Generate sidebar links
        """
        className = None
        if selected and self.src == selected:
            className = "selected"
        if className:
            html = "<li class='menuitem "+className+"'><span><a href='"+self.href(depth)+"'>"+self.title()+"</a></span></li>\n"
        else:
            html = "<li class='menuitem'><span><a href='"+self.href(depth)+"'>"+self.title()+"</a></span></li>\n"
        return html

    def html(self, sidebar, depth):
        """
        Generate html document for each md document
        """
        with open(self.src, "r") as in_file:
            html = markdown2.markdown(in_file.read())
        sidebar.set_selected(self.src)
        sidebar_html = sidebar.html(depth)
        print("Writing {}".format(self.dest))
        with open(self.dest, "w") as out_file:
            out_file.write(self.section.theme.header)
            out_file.write(sidebar_html)
            out_file.write("<main>\n")
            out_file.write(html)
            out_file.write("</main>")
            out_file.write(self.section.theme.footer)


class Sidebar:
    """
    Sidebar class that contains methods and information that is required to 
    generate a sidebar on a per document basis.
    """
    def __init__(self, sections):
        self.sections = sections
        self.template = None
        self.selected = None

    def set_selected(self, file:str):
        self.selected = file

    def html(self, depth):
        print("Sidebar::html {}".format(len(self.sections)))
        html_text = "<aside>\n"
        html_text += "<ul class='collapsible'>\n"
        for section in self.sections:
            html_text += section.links_entry(self.selected, depth)
        html_text += "</ul>"
        html_text += "</aside>\n"
        return html_text

class Theme:
    def __init__(self, path):
        self.title = "Documentation"
        self.header_template = None
        self.footer_template = None
        self.sidebar_template = None
        self.sections = []
        self.sidebar = None
        header_path = os.path.join(path, "header.html")
        if os.path.isfile(header_path):
            with open(header_path, "r") as in_file:
                self.header = in_file.read()
        footer_path = os.path.join(path, "footer.html")
        if os.path.isfile(footer_path):
            with open(footer_path, "r") as in_file:
                self.footer = in_file.read()
        sidebar_path = os.path.join(path, "sidebar.html")
        if os.path.isfile(sidebar_path):
            with open(footer_path, "r") as in_file:
                self.sidebar_template = in_file.read()

    def add_section(self, section):
        self.sections.append(section)

    def section(self):
        return self.sections[-1]

    def generate(self):
        # iterate through all links and add them to our sidebar
        print("Theme")
        for section in self.sections:
            section.before()
        self.sidebar = Sidebar(self.sections)
        self.sidebar.template = self.sidebar_template
        for section in self.sections:
            section.html(self.sidebar,0)
     
def main_config(file: str):
    """
    Load the main configuration file.
    """
    if not os.path.isfile(file):
        raise ValueError("{} is not a valid file".format(file))
    # you can add defaults into this dict structure
    contents = {"theme":"plain"}
    with open(file, "r") as config_file:
        contents.update(json.load(config_file))
    return contents


def load_theme(theme_name):
    """
    Load the theme from a directory
    """
    theme_path = os.path.join(themes_dir, theme_name)
    theme = Theme(theme_path)
    return theme


def build_documents(theme: Theme, section: Section, build_dir: str, cur_dir: str):
    """
    Recurse through the docs_dir directory
     - handle img directories
     - handle other directories as document containers
     - handle config.json files
     - handle md files
     - handle other files
    """
    if not os.path.isdir(cur_dir):
        raise ValueError("{} is not a valid directory".format(cur_dir))
    if not os.path.isdir(build_dir):
        os.makedirs(build_dir)
    with os.scandir(cur_dir) as it:
        for entry in it:
            if entry.is_dir():
                if entry.name == image_dir:
                    shutil.copytree(os.path.join(cur_dir, entry.name), os.path.join(
                        build_dir, entry.name), dirs_exist_ok=True)
                else:
                    new_section = Section(theme)
                    new_section.name = convert_filename_to_title(entry.name)
                    build_documents(theme,new_section, os.path.join(
                        build_dir, entry.name), os.path.join(cur_dir, entry.name))
                    section.add_section(new_section)
            elif entry.is_file():
                if entry.name != "." and entry.name != "..":
                    if entry.name == "config.json":
                        section.set_config(os.path.join(cur_dir,entry.name))
                    else:
                        (name, ext) = os.path.splitext(entry.name)
                        if ext == ".md":
                            section.add(os.path.join(
                                cur_dir, entry.name), os.path.join(build_dir, name+".html"))
                        else:
                            shutil.copyfile(os.path.join(cur_dir,entry.name),os.path.join(build_dir,entry.name))
                            

def main():
    """
    The application starts here and processes the loads the config.json file.
    """
    config = main_config(os.path.join(".", config_file))
    theme = load_theme(config["theme"])
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    # a root section is added that will be hidden in the javascript contents
    section = Section(theme)
    section.name = "__top__"
    build_documents(theme, section, build_dir, docs_dir)
    theme.add_section(section)
    # copy over the img directory verbatim
    shutil.copytree(os.path.join(".", image_dir), os.path.join(
        build_dir, image_dir), dirs_exist_ok=True)
    # finally we generate the static files
    theme.generate()

if __name__ == "__main__":
    main()
