# University of Highlands and Islands BSc Applied Software Development Degree Knowledge Base

This repository is for students on the BSc Applied Software Development degree to create their own body of knowledge, both to help themselves but also future students.

Visit us to learn more about our degree and studying at the [University of Highlands and Islands](https://www.uhi.ac.uk/en/courses/bsc-hons-applied-software-development/).

## Prerequisites

```
apt-get -y install python3
pip install markdown2
```
## Generate fake documents

Use the fake_docs.sh script to generate 400 fake documents to add to the menu.  It should handle these nicely.

```
./fake_docs.sh
```

## Build

The final set of documents is created in the build directory.  It will copy any none markdown files directly across.  The outer img directory will also be copied across in its entirety.

```
python3 build.py
```

## Configuration

Configuration is handled using JSON files called 'config.json'. There is a root config.json that should contain information about the theme and high level configuration information that applies to all documents.  If a config.json appears in a docs directory, it will set local configuration such as item order.

## Contribute

1. Clone the repository
2. Add some articles
3. Request a pull request following the instructions [https://www.atlassian.com/git/tutorials/making-a-pull-request](https://www.atlassian.com/git/tutorials/making-a-pull-request).

## Clone the repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).