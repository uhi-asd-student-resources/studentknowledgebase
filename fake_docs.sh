#!/bin/bash

# Generates fake documents
for ii in $(seq 1 20); do
    mkdir -p docs/fake-docs$ii
    for jj in $(seq 1 20); do
        echo "# header for docs $ii - $jj" > docs/fake-docs$ii/fake-document$jj.md
    done
done