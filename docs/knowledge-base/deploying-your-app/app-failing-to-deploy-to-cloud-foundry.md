# App failing to deploy to cloud foundry

If your app is failing at the deploy stage when deploying to the Cloud Foundry, ensure the "Application Name" does not have any spaces in it.

If it fails because of a lack of memory, then check you have the Academic Initiative Feature Code added to your Account.  Seek assistance from a delivery staff member if you need help.