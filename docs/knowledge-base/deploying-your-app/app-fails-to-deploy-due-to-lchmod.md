# App fails to deploy due to lchmod (file attributes) error: Not supported

If you get the error when trying to push your app to the Cloud Foundry called:

```
lchmod (file attributes) error: Not supported
```

Check your inputs into the build script as you might have copied over lines which are not required.  If you still cannot get it to work, remove one line at a time until you isolate the one that is causing the issue.