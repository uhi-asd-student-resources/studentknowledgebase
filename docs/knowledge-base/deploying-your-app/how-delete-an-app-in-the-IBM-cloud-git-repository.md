# How to delete an app in the IBM Cloud Git Repository

When you are first starting its easy to create new repositories that you might not want to keep.  To remove a repository can be quite hard to find so here are some easy steps to find the right button.


Navigate to https://eu-gb.git.cloud.ibm.com/ and login under your user.
* Select the repository you want to delete
* Go to Settings
* Click on General
* Click on Advanced at bottom of this screen
* Scroll right to bottom
* Click on Remove Project (big red button)
* You will need to type the name of the repository to confirm removal.
