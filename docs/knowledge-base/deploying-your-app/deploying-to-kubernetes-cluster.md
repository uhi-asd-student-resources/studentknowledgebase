# Deploying to Kubernetes Cluster

If you are having trouble connecting to a kubernetes cluster check the following:

1. Check you have the Container Registry plugin installed:
```
ibmcloud plugin update container-registry
```

2. Check you are logged in under the Container Registry. You need to be logged in both with the cloud service (ibmcloud login) AND the Container Registry login.
```
ibmcloud cr login
```

3. Check you have a namespace created with only numbers, characters and underscores.

```
ibmcloud cr namespace-list
```

To add a new namespace add:

```
ibmcloud cr namespace-add <your-namespace-name>
```

4. Check you have updated the cli-config.yaml file with the information suggested by [https://cloud.ibm.com/docs/cli?topic=cloud-cli-idt-cli](https://cloud.ibm.com/docs/cli?topic=cloud-cli-idt-cli) under "Deploying to IBM Cloud Kubernetes Service".


Below is a working configuration, where mycluster is the name of the kubernetes cluster you have created.

```
chart-path: "chart/myapp"
deploy-target: "container"
deploy-image-target: "uk.icr.io/myapp"
ibm-cluster: "mycluster"
```