# Webpage is not updating when changing file in 'frontend' directory of project

Use the following command to copy all the compiled versions in your frontend directory to the public area used by the deployment scripts.

```
cp -R frontend/dist/* public/
```

This assumes you have carried out the following commands in the 'frontend' directory to rebuild your distribution.

```
cd frontend
yarn install
yarn build
```
