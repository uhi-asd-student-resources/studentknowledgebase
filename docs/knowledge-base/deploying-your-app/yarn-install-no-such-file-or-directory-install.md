# Parsing scenario file install ERROR: [Errno 2] No such file or directory: 'install'

When running 'yarn install' on a project you might come across the error:

```
Parsing scenario file install
ERROR: [Errno 2] No such file or directory: 'install'
```

This is because if you are using an old version of Ubuntu the yarn version is old.  You will need to carry out the following commands:

```
sudo apt remove cmdtest
sudo apt remove yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update  
sudo apt-get install yarn
yarn --version
```