# Setting the target to run your project

Most of the time you will want to run your project, based on a starter kit, on Cloud Foundry.  Cloud Foundry is a public Kubernetes cluster on IBM Cloud.

To target this cluster type:

```
ibmcloud target -cf
```

