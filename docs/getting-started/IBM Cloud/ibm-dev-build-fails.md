# "ibm dev build" fails

## Could not resolve host: cdn-ubi.redhat.com

Example error:

```
Sending build context to Docker daemon  2.048kB
Step 1/3 : FROM registry.access.redhat.com/ubi8/ubi
 ---> ecbc6f53bba0
Step 2/3 : RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
 ---> Using cache
 ---> 5ecf9f400c5b
Step 3/3 : RUN yum install -y nodejs
 ---> Running in 8712b2c5de4c
Updating Subscription Management repositories.
Unable to read consumer identity
This system is not registered to Red Hat Subscription Management. You can use subscription-manager to register.
Red Hat Universal Base Image 8 (RPMs) - BaseOS  0.0  B/s |   0  B     01:38    
Errors during downloading metadata for repository 'ubi-8-baseos':
  - Curl error (6): Couldn't resolve host name for https://cdn-ubi.redhat.com/content/public/ubi/dist/ubi8/8/x86_64/baseos/os/repodata/repomd.xml [Could not resolve host: cdn-ubi.redhat.com]                                                                                                                                                          
Error: Failed to download metadata for repo 'ubi-8-baseos': Cannot download repomd.xml: Cannot download repodata/repomd.xml: All mirrors were tried      
```

### Why

Docker is no longer passing the correct network settings to your containers.

### Solution

Restart your docker instance:

```
sudo systemctl docker restart
```

Check your network is ok:

```
docker network ls
```

Should be something like:

```
NETWORK ID          NAME                DRIVER              SCOPE
6e22e8cbbc0e        bridge              bridge              local
661bc51b6c12        host                host                local
f4f4ac750f54        none                null                local
```