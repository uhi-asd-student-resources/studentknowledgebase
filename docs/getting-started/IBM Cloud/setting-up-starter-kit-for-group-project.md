# How to setup your starterkit to read from a central repo using local services

The problem with starter kits as they come is that the manifest.yml file specifies a named resource that you need to have in your own cloud account.  Using a toolchain that reads from a central repository will lead to an error.

## The solution

1. Take note of the name of the Cloud Foundry Service that your starter kit creates for Cloudant.

![Cloud Foundry Cloudant Service](./img/ibmcloud_cloud_foundry_service.png  "Cloud Foundry Cloudant Service")

2. In your toolchain, click on Deliver.
3. Click on Actions... and then Configure Pipeline.

![Configure Pipeline](./img/1.png  "Configure Pipeline")


4. Under Environment Pro... click on Add Property.

![Add Property](./img/2.png  "Add Property")

5. Select "Text Property".
6. In the left hand box type: SERVICE_ALIAS.
7. In the right hand box paste the name obtained in step 1.
8. Ad another "Text Property".
9. In the left hand box type: APP_NAME.
10. In the right hand box paste the name of the app (must be unique on Cloud Foundry).
11. Click Save.
12. Click on "Add Stage" and select "Build Stage".

![Build Stage](./img/5.png  "Build Stage")

10. Once the stage is created, move to between the BUILD and DEPLOY stages.

![Move between BUILD and DEPLOY stages](./img/6.png  "Move between BUILD and DEPLOY stages")

11. Click on DEPLOY wheel and Configure Stage.
12. Click on Inputs tab.
13. Under Input Settings > Stage select MyStage.

![Select MyStage as precursor to DEPLOY](./img/11.png  "Select MyStage as precursor to DEPLOY")

14. Click Save.
15. For the MyStage stage, click on Configure Stage.
16. Select "Jobs" tab.
17. Click on "Add Job".
18. Select "Shell Script"

![Select Shell Script](./img/6.png  "Select Shell Script")

19. Then copy the following in the text box:

![Paste in bash script](./img/8.png  "Paste in bash script")

```
#!/bin/bash
# your script here
echo "Working directory:"
echo $(pwd)
echo "Directory:"
ls
echo "Before change manifest.yml is:"
cat manifest.yml

# This modifies the manifest.yml to talk to your
# local cloudant service rather than one owned by the 
# original creator.
# Reference: sed command line tool
sed -i "s/- cloudant-.*/- ${SERVICE_ALIAS}/" manifest.yml

# In the manifest.yml, replace the route if it exists
# - route: testproject1.eu-gb.cf.appdomain.cloud
sed -i "s/- route: .*/- route: ${APP_NAME}.eu-gb.cf.appdomain.cloud/" manifest.yml

# In the manifest.yml we should also update the name.
sed -i "s/name: .*/name: ${APP_NAME}/" manifest.yml

echo
echo "After change manifest.yml is:"
cat manifest.yml
```

20. In the "Archive Directory", type in a dot ('.').
21. Click Save.
22. Run from the first stage and ensure this pipeline works.


## What if you want to change the command line deployment?

If you are using ```ibmcloud dev deploy``` to deploy your solution for testing you should be able to make the following changes.

1. Ensure that manifest.yml is in the file .gitignore.  If it isn't then add it.
2. Modify manifest.yml with the same changes as in the script.  This is change the "name:", "- route" and "- cloudant" lines to reflect your specific cloud resources.
3. Save this file.
4. Now use ```ibmcloud dev deploy``` as usual.

Deploying this way is likely to create a second Cloud Foundry app but using the same local services, which is not quite what you want.

An easier way though is instead of using ```ibmcloud dev deploy```, you can use ```ibmcloud dev pipeline-run <pipeline-id>``` where you get the id from the url of the delivery stage of the toolchain e.g. https://cloud.ibm.com/devops/pipelines/**faf2e92b-0b86-4093-97c8-eddfb1358ec6**?env_id=ibm:yp:eu-gb.
