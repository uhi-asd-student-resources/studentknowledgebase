# About the IBM Container Registry

If you are using Kubernetes to host your application you will need to understand the IBM Container Registry. This is a storage area for your docker images for Kubernetes to spin up.

Each individual user or company has a "space" where they can store their docker images. This space can be split into at least 1 namespace. Namespaces are unique to geography. A common use case is to have 1 namespace for development and one for production, or one in Europe and one for US with different locale settings.

You control the container registry from the command line using the container-registry plugin.

```
ibmcloud plugin install container-registry
```

To list your images use:

```
ibmcloud cr image-list
```

Destroying the namespace, destroys all images within it.
If you have problems deleting an image, it may be possible to delete the whole namespace.

```
ibmcloud cr image-rm <image>
```

The amount of space that you get under your academic license is only 512 MB with a pull traffic of 5GB so you may need to remove older projects before starting new ones.

```
$ ibmcloud cr quota
Getting quotas and usage for the current month, for account '<your name> Account'...

Quota          Limit    Used   
Pull traffic   5.0 GB   0 B   
Storage        512 MB   0 B   

OK
```

Use the following to see all the options for container registry:

```
ibmcloud cr help
```

Reference: [https://cloud.ibm.com/docs/services/Registry?topic=registry-registry_setup_cli_namespace](https://cloud.ibm.com/docs/services/Registry?topic=registry-registry_setup_cli_namespace)