# Introduction

This repository is a public repository with articles created by by students and staff as part of the UHI Applied Software Development degree launched in September 2020.

If you are interested in learning more visit our [University website](https://www.uhi.ac.uk/en/courses/bsc-hons-applied-software-development/).

View this knowledge base at [http://asd.uhi.ac.uk/kb](http://asd.uhi.ac.uk/kb).
