# Pull Request Ettiquette

These are some lessons learned from a talk by Mike Cowan at Adobe in 2020.


* A PR should communicate the story of your contribution to another engineer as clearly as possible
    * The story includes **what** the change is, **why** you want to make the change and **how** it was achieved
* Projects should standardise their PRs by specifying conventions for (with examples)
  * Branch name – grouped, include a bug number
    * Should be easy to read at 3am in the morning!
    * Bad: BUG-1111_1
    * Good: fix/BUG-1111/DatabaseHandler-callback-not-triggered
  * Title – describe the what, limit to 72 characters
    * Do not say why or how, only **what**
    * Bad: "fix to trigger the callback when a property is removed in DatabaseCheck"
    * Good: "Fix handling of a removed property"
  * Description – describe the why (and maybe what), use Mark Down, be expressive
    * Here is where you express **why** and expand on the **what**
    * Bad: ""
    * Good: "DatabaseHandler is designed to trigger a callback when a property is changed. The fix here is to trigger the callback when a property is cleared or removed."
  * Commits – describe the how, limit to 50 characters, start with a capital, no period at the end
    * sequential commit history that shows a logical progression of work.
    * use "rebase -i" to squash and rewrite commit history to reflect work
    * do not rewrite commits made in reaction to review comments as that confuses reviewers.
  * Language – imperative mood (a command, start with a verb) and present tense
* Hygiene – delete branch once PR is merged


In this scenario a branch is never manually merged with the master branch.  This happens as part of the CI/CD pipeline only **AFTER** all checks have been completed and the branch has been pushed live.  

The Pull Request process at Adobe follows this pattern:

1. Authorisation - do you have permission to open a pull request?
2. Build pipeline - checks limits on diff (e.g. cannot change more than 5 files at a time), build, static analysis tools, tests, test coverage reports
3. Code Review - sent to colleagues to review and understand the contribution, ensure its functional, request changes, check standards have been applied, and approve
4. Deployment pipeline - the change is pushed to at least the stage environment
5. Squash commits - all commits on a branch are squashed down to 1 using rebase so that the master branch commit history is clean and easy to view
6. Merge - merge the branch on to the production branch (frequently called 'master' or 'main')
