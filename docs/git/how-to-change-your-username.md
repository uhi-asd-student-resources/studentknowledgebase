# How to change your username or email address throughout a repository?

This will help if you have accidentally got a wrong username in your commit.  This is to be used as a last resort.

Another common case is that you forgot to run git config to set your name and email address before you started working, or perhaps you want to open-source a project at work and change all your work email addresses to your personal address. In any case, you can change email addresses in multiple commits in a batch with filter-branch as well. You need to be careful to change only the email addresses that are yours, so you use --commit-filter:

```
git filter-branch --commit-filter '
        if [ "$GET_AUTHOR_NAME" = "schacon" ];
        then
                GIT_AUTHOR_NAME="Scott Chacon";
                GIT_AUTHOR_EMAIL="schacon@example.com";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD
```

**WARNING: This goes through and rewrites every commit to have your new address. Because commits contain the SHA-1 values of their parents, this command changes every commit SHA-1 in your history, not just those that have the matching email address.**

## Reference

* https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History